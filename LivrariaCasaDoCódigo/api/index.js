
// Criando o Servidor para a API

const restify = require('restify');
const erros = require('restify-errors');

const server = restify.createServer({
    name: "API - Livraria"
});

const knex = require('knex')({
    client: 'pg',
    version: '10.6',
    connection: {
        host : '127.0.0.1',
        user : 'aps',
        password : 'aps',
        database : 'livraria'
    }
});

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

server.listen(4000, function () {
    console.log("%s Servidor Ativo - Livraria - Casa do Código - %s", server.name, server.url);
});

// Criando requisições GET

server.get('/', function (req, res, next) {
    let mensagem = {
        text: "Bem vindo - API - Livraria"
    };
    res.json(mensagem);
    next();
});

// Select All - Livros

server.get('/livros', (req, res, next) =>  {

    knex('livros').then((dados) =>{
        res.send(dados);
    }, next)

});

// Adiciona um novo livro

server.post('/livros/adiciona', (req, res, next) =>  {

    knex('livros')
        .insert(req.body)
        .then((dados) =>{
        res.send(dados);
    }, next)

});

server.get('/livros/buscar/:id', (req, res, next) =>  {

    const { id } = req.params;

    knex('livros')
        .where('id_livros', id)
        .first()
        .then((dados) =>{
            if(!dados) return res.send(new erros.BadRequestError("Livro não encontrado!"))
            res.send(dados);
    }, next)

});

server.put('/livros/atualiza/:id', (req, res, next) =>  {

    const { id } = req.params;

    knex('livros')
        .where('id_livros', id)
        .update(req.body)
        .then((dados) =>{
            if(!dados) return res.send(new erros.BadRequestError("Livro não encontrado!"))
            res.send("Dados Atualizados");
        }, next)

});

server.del('/livros/delete/:id', (req, res, next) =>  {

    const { id } = req.params;

    knex('livros')
        .where('id_livros', id)
        .delete()
        .then((dados) =>{
            if(!dados) return res.send(new erros.BadRequestError("Livro não encontrado!"))
            res.send("Dados Exluídos");
        }, next)

});


