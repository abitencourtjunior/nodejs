class ClienteDao{

  constructor(db){
    this._db = db;
  }

  lista(){
    const new Promise((resolve, reject) =>{
      this._db.all('select * from clientes;',
        (error, result) =>{
          if (error) return reject("Não foi possível buscar clientes!");

          return resolve(result);
        });
    });
  }
}
